provider "aws" {
  region = "${var.aws_region}"
}

terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "qdi"

    workspaces {
      name = "wordpress-eks"
    }
  }
}

locals {
  cluster_name = "wp-eks-${random_string.suffix.result}"
}

resource "random_string" "suffix" {
  length  = 8
  special = false
}

data "aws_availability_zones" "available" {
}

module "vpc" {
  source             = "terraform-aws-modules/vpc/aws"
  name               = "wp-vpc"
  cidr               = "10.0.0.0/16"
  azs                = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  private_subnets    = ["10.0.1.0/24", "10.0.2.0/24"]
  public_subnets     = ["10.0.101.0/24", "10.0.102.0/24"]
  enable_nat_gateway = true
  single_nat_gateway = true
  create_database_subnet_group           = true
  create_database_subnet_route_table     = true
  create_database_internet_gateway_route = true
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Owner       = "user"
    Environment = "dev"
  }
}

module "eks" {
  source                = "terraform-aws-modules/eks/aws"
  cluster_name          = "wp-eks"
  subnets               = module.vpc.public_subnets
  vpc_id                = module.vpc.vpc_id
  write_kubeconfig      = true
  write_aws_auth_config = false

  worker_groups = [
    {
      instance_type         = "t2.small"
      asg_desired_capacity  = "2"
      asg_max_size          = "5"
      asg_force_delete      = true
      enable_monitoring     = true
      autoscaling_enabled   = true
      protect_from_scale_in = true
    }]
}

module "rds" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"

  identifier = "wp-db"

  engine            = "mysql"
  engine_version    = "5.7.22"
  instance_class    = "db.t2.micro"
  allocated_storage = 5

  name     = "wordpress"
  username = "wp"
  password = "12345Abcde"
  port     = "3306"

  publicly_accessible = true

  iam_database_authentication_enabled = true

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"
  apply_immediately = true

  tags = {
    Owner       = "user"
    Environment = "dev"
  }

  skip_final_snapshot = true
  create_db_option_group = false

  # DB subnet group
  subnet_ids = module.vpc.public_subnets

  # DB parameter group
  family = "mysql5.7"

  parameters = [
    {
      name = "character_set_client"
      value = "utf8"
    },
    {
      name = "character_set_server"
      value = "utf8"
    }
  ]
}