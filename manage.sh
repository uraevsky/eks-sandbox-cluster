#!/usr/bin/env sh
CLUSTER_NAME="wp-eks"
AWS_REGION="eu-west-1"

create() {
        # Validate
    terraform validate

    terraform apply -auto-approve -target=data.aws_availability_zones.available
        # Deploy VPC first
    terraform apply -auto-approve -target=module.vpc
        # Deploy EKS second
    terraform apply -auto-approve -target=module.eks
        # Get kubeconfig to be used by all further commands
    aws eks update-kubeconfig --name $CLUSTER_NAME --region=$AWS_REGION

        # Let system pods be created first
    sleep 30
        # Now wait for system pods to fully come up
    kubectl -n kube-system wait pod --for=condition=Ready --timeout=120s \
        -l 'k8s-app in (kube-proxy, aws-node, kube-dns)'

        # Create tiller service account
        # TODO: remove this after upgrade to Helm 3
    kubectl apply -f tiller.yaml

        # Deploy DB
    terraform apply -auto-approve -target=module.rds

        # Deploy everything else
    terraform apply -auto-approve
}

delete() {
  terraform destroy -auto-approve  
}

case "$1" in
    "create" | "delete" )
        $1
        ;;
    *)
        echo "Wrong arguments"
        exit 1
esac