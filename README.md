# Example for to see how work base modules of AWS

Must Have: EC2, IAM, S3, ELB/ALB, CloudWatch, VPC and Networking, RDS, CloudFront, Deployment Tools.

I create this examlpe to show how i can use "must have" services of AWS

Good to have: ECS, EMR, SQS, API GW, SSM, others

# Usage

to create cluster `./manage.sh create`  
to delete cluster `./manage.sh delete`