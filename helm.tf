data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = module.eks.cluster_endpoint
  cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
}


# Initialize Helm provider

provider "helm" {
  service_account = "tiller"
  kubernetes {
    config_path = module.eks.kubeconfig_filename
  }
}

resource "helm_release" "cluster-autoscaling" {
  name      = "cluster-autoscaler"
  namespace = "kube-system"
  chart     = "stable/cluster-autoscaler"

  set {
    name  = "autoDiscovery.clusterName"
    value = "eks"
  }

  set {
    name  = "autoDiscovery.enabled"
    value = "true"
  }

  set {
    name  = "cloudProvider"
    value = "aws"
  }

  set {
    name  = "awsRegion"
    value = var.aws_region
  }

  set {
    name  = "sslCertPath"
    value = "/etc/kubernetes/pki/ca.crt"
  }

  set {
    name  = "rbac.create"
    value = "true"
  }
}

resource "helm_release" "wp-dix" {
  name      = "wordpress"
  namespace = "wp-deploy"
  chart     = "stable/wordpress"

  values = [<<EOF
wordpressUsername: dix
wordpressEmail: uraevsky@gmail.com
wordpressFirstName: Mihail
wordpressLastName: Uraevsky
wordpressBlogName: Mihail Uraevsky Blog!
replicaCount: 1
externalDatabase:
  host: ${module.rds.this_db_instance_address}
  user: wp
  password: "12345Abcde"
  database: wordpress
  port: 3306
mariadb:
  enabled: false
EOF
  ]
}